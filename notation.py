from analyse import *
from constants import *
import random
import statistics

def initialise(db, ride_id, car_id):
    time, v, rpm, fuel, alat = db.getMetrics(ride_id, car_id)
    afor, t = derivate(time, v, K)
    return time, v, rpm, afor, alat, fuel

def fuel_consumption(time, v, fuel):
    counter_fuel = 0
    previous_fuel_consumption = (FUEL_LIMIT_INF + FUEL_LIMIT_SUP) / 2
    for i in range(1, len(fuel)):
        time_step = (time[i] - time[i-1])/K
        distance_step = v[i] * time_step
        if distance_step != 0:
            fuel_consumption = (fuel[i] - fuel[i-1]) * FUEL_CAPACITY / distance_step
            if (fuel_consumption >= FUEL_LIMIT_SUP) and (fuel_consumption < FUEL_LIMIT_SUP):
                counter_fuel += 1
            if (fuel_consumption <= FUEL_LIMIT_INF) and (fuel_consumption > FUEL_LIMIT_INF):
                counter_fuel -= 1
            if counter_fuel <= 0:
                counter_fuel = 0
            previous_fuel_consumption = fuel_consumption
    return counter_fuel

def acceleration_counter(afor):
    counter_aforpos = 0
    for i in range(1, len(afor)):
        if afor[i] >= FORWARD_ACCELERATION_LIMIT and afor[i-1] < FORWARD_ACCELERATION_LIMIT:
            counter_aforpos += 1
    return counter_aforpos

def engine_rotation(rpm):
    counter_rpm = 0
    for i in range(1, len(rpm)):
        if rpm[i] >= RPM_LIMIT and rpm[i-1] < RPM_LIMIT:
            counter_rpm += 1
    return counter_rpm

def eff_marks(afor, fuel, time, v, rpm):
    n = len(time)
    counter_afor = acceleration_counter(afor)
    counter_rpm = engine_rotation(rpm)
    counter_fuel = fuel_consumption(time, v, fuel)
    a_mark = FORWARD_ACCELERATION_COUNTER_0-FORWARD_ACCELERATION_COUNTER_1*(counter_afor/n)
    fuel_mark = FUEL_COUNTER_0-FUEL_COUNTER_1*(counter_fuel/n)
    rpm_mark = RPM_COUNTER_0-RPM_COUNTER_1*(counter_rpm/n)
    eff = a_mark + fuel_mark + rpm_mark
    EFF_0 = FORWARD_ACCELERATION_COUNTER_0 + FUEL_COUNTER_0 + RPM_COUNTER_0
    EFF_1 = FORWARD_ACCELERATION_COUNTER_1 + FUEL_COUNTER_1 + RPM_COUNTER_1
    eff = (1 + eff/(EFF_0 + EFF_1))*0.5
    a_mark = 0.5 + 0.5*a_mark/(FORWARD_ACCELERATION_COUNTER_0 + FORWARD_ACCELERATION_COUNTER_1)
    fuel_mark = 0.5 + 0.5*fuel_mark/( FUEL_COUNTER_0 +  FUEL_COUNTER_1)
    rpm_mark = 0.5 + 0.5*rpm_mark/(RPM_COUNTER_0 + RPM_COUNTER_1)
    return a_mark, fuel_mark, rpm_mark, eff

def lateral_acc(alat):
    counter_alat = 0
    for i in range(1, len(alat)):
        if alat[i] >= LATERAL_ACCELERATION_POSITIVE_LIMIT and alat[i-1] < LATERAL_ACCELERATION_POSITIVE_LIMIT:
            counter_alat += 1
        if alat[i] <= LATERAL_ACCELERATION_NEGATIVE_LIMIT and alat[i-1] > LATERAL_ACCELERATION_NEGATIVE_LIMIT:
            counter_alat += 1
    return counter_alat

def dangerous_acc(afor):
    counter_dang_afor = 0
    for i in range(1, len(afor)):
        if abs(afor[i]) <= DANGEROUS_ACCELERATION_LIMIT and abs(afor[i-1]) > DANGEROUS_ACCELERATION_LIMIT:
            counter_dang_afor += 1
    return counter_dang_afor

def negative_acc(afor):
    counter_neg_afor = 0
    for i in range(1, len(afor)):
        if afor[i] <= NEGATIVE_ACCELERATION_LIMIT and afor[i-1] > NEGATIVE_ACCELERATION_LIMIT:
            counter_neg_afor += 1
    return counter_neg_afor

def speed_counter(v):
    counter_v = 0
    for i in range(1, len(v)):
        if v[i] >= SPEED_LIMIT and v[i-1] < SPEED_LIMIT:
            counter_v += 1
    return counter_v

def driven_time(time):
    counter_t = 0
    for i in range(1, len(time)):
        if time[i] >= TIME_LIMIT and time[i-1] < TIME_LIMIT:
            counter_t = 1
    return counter_t

def sec_marks(alat, afor, time, v):
    t = max(time)
    alat_mark = LATERAL_ACCELERATION_COUNTER_0-LATERAL_ACCELERATION_COUNTER_1*(lateral_acc(alat)/t)
    dang_acc_mark = DANGEROUS_ACCELERATION_COUNTER_0-DANGEROUS_ACCELERATION_COUNTER_1*(dangerous_acc(afor)/t)
    neg_acc_mark = NEGATIVE_ACCELERATION_COUNTER_0-NEGATIVE_ACCELERATION_COUNTER_1*(negative_acc(afor)/t)
    v_mark = SPEED_COUNTER_0-SPEED_COUNTER_1*(speed_counter(v)/t)
    SEC_0 = LATERAL_ACCELERATION_COUNTER_0 + DANGEROUS_ACCELERATION_COUNTER_0 + NEGATIVE_ACCELERATION_COUNTER_0 + SPEED_COUNTER_0
    SEC_1 = LATERAL_ACCELERATION_COUNTER_1 + DANGEROUS_ACCELERATION_COUNTER_1 + NEGATIVE_ACCELERATION_COUNTER_1 + SPEED_COUNTER_1
    sec = alat_mark + dang_acc_mark + neg_acc_mark + v_mark
    if driven_time(time) != 0:
        sec -= TIME_COUNTER_0
        SEC_0 += TIME_COUNTER_0
    sec = (1 + sec/(SEC_0 + SEC_1))*0.5
    alat_mark = 0.5 + 0.5*alat_mark/(LATERAL_ACCELERATION_COUNTER_0 + LATERAL_ACCELERATION_COUNTER_1)
    dang_acc_mark = 0.5 + 0.5*dang_acc_mark/(DANGEROUS_ACCELERATION_COUNTER_0 + DANGEROUS_ACCELERATION_COUNTER_1)
    neg_acc_mark = 0.5 + 0.5*neg_acc_mark/(NEGATIVE_ACCELERATION_COUNTER_0 + NEGATIVE_ACCELERATION_COUNTER_1)
    v_mark = 0.5 + 0.5*v_mark/(SPEED_COUNTER_0 + SPEED_COUNTER_1)
    return alat_mark, dang_acc_mark, neg_acc_mark, v_mark, sec

def driving_marks(db, ride_id, car_id):
    time, v, rpm, afor, alat, fuel = initialise(db, ride_id, car_id)
    a_mark, fuel_mark, rpm_mark, eff = eff_marks(afor, fuel, time, v, rpm)
    alat_mark, dang_acc_mark, nag_acc_mark, v_mark, sec = sec_marks(alat, afor, time, v)
    driving_mark = (eff+sec)/2
    return a_mark, fuel_mark, rpm_mark, eff, alat_mark, dang_acc_mark, nag_acc_mark, v_mark, sec, driving_mark

def evaluation(mark):
    if mark <= 0.2:
        return "Very bad driving. Driver needs to improve immediately!"
    elif 0.2 < mark <= 0.4:
        return "Bad driving. Driver must improve."
    elif 0.4 < mark <= 0.6:
        return "Regular driving, however it could be improved."
    elif 0.6 < mark <= 0.8:
        return "Good driving, although it could be slightly improved."
    else:
        return "Excellent driving!"