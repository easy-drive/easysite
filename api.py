from flask import *
from config import *
from utils import *
from database import DataBase
import os
import gzip
import pyrebase

api = Flask(__name__)
api.secret_key = os.urandom(24)

firebase = pyrebase.initialize_app(firebaseConfig)
auth = firebase.auth()

@api.route('/', methods=['GET'])
def index():
      admin = False
      try:  
            if session['usr_id']:
                  db = DataBase()
                  user_id, user_name, user_email = db.getUserInfo(session['usr_id'])[0]
                  db.close()
                  if user_email in adminList:
                        admin = True
                  return render_template("index.html", admin=admin)
      except Exception as e:
            print(e)
      return render_template("index.html", admin=admin)

@api.route('/profile', methods=['GET'])
def profile():
      try:  
            if session['usr_id']:
                  admin = False
                  db = DataBase()
                  user_id, user_name, user_email = db.getUserInfo(session['usr_id'])[0]
                  db.close()
                  if user_email in adminList:
                        admin = True
                  return render_template("profile.html", name=user_name, user_id=user_id, admin=admin)
      except Exception as e:
            print(e)
            return redirect(url_for("login"))

@api.route('/login', methods=['GET'])
def login():
      try:
            if session['usr_id']:
                  return redirect(url_for('profile'))
      except Exception as e:
            print(e)
      try:
            login = json.loads(request.args['failed'])['login']
            message = ""
            if login:
                  message = ("login", "Invalid email/password combination")
            else:
                  message = ("signup", "Error when registering, please try again")
            return render_template("login.html", message=message, login=login)
      except Exception as e:
            print(e)

      return render_template("login.html", login=True)

@api.route('/logout', methods=['GET'])
def logout():
      try:
         session.clear()   
      except Exception as e:
            print(e)
      return redirect(url_for('index'))

@api.route('/database', methods=['GET'])
def db():
      try:
            if session['usr_id']:
                  db = DataBase()
                  user_id, user_name, user_email = db.getUserInfo(session['usr_id'])[0]
                  if user_email in adminList:
                        tables = db.getTables()
                        return render_template("database.html",title="Database",tables=tables,admin=True)
                  else:
                        abort(401)
                  db.close() 
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/visualize', methods=['GET'])
def visualize():
      try:
            if session['usr_id']:
                  db = DataBase()
                  user_id, user_name, user_email = db.getUserInfo(session['usr_id'])[0]
                  db.close()
                  if user_email in adminList:
                        return render_template("visualize.html", title="Data Visualization",admin=True)
                  else: 
                        abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/get_data', methods=['POST'])
def get_data():
      try:
            if session['usr_id']:
                  data_base = DataBase()
                  user_id, user_name, user_email = data_base.getUserInfo(session['usr_id'])[0]
                  if user_email in adminList:
                        return profiles(request, data_base)
                  data_base.close()
            else: 
                  abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/get_database',methods=['POST'])
def get_db():
      try:
            if session['usr_id']:
                  data_base = DataBase()
                  user_id, user_name, user_email = data_base.getUserInfo(session['usr_id'])[0]
                  if user_email in adminList:
                        return getDb(request, data_base)
                  data_base.close()
            else: 
                  abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/process',methods=['POST'])
def process_data():
      try:
            if session['usr_id']:
                  data_base = DataBase()
                  user_id, user_name, user_email = data_base.getUserInfo(session['usr_id'])[0]
                  if user_email in adminList:
                        return processDb(request, data_base)
                  data_base.close()
            else: 
                  abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/marks',methods=['POST'])
def marks():
      try:
            if session['usr_id']:
                  data_base = DataBase()
                  return sendMarks(request, data_base)
            else: 
                  abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/grades',methods=['POST'])
def grades():
      try:
            data_base = DataBase()
            return getMarks(request, data_base)
      except Exception as e:
            print(e)
            return "None"

@api.route('/api/dashboard',methods=['POST'])
def dashboard():
      try:
            if session['usr_id']:
                  request_data = request.get_json()
                  user_id = request_data['user_id']
                  data_base = DataBase()
                  cars = data_base.getUserProfile(user_id)
                  car_id = cars['data'][0][0]
                  last_rides = data_base.getLastRide(car_id)
                  if len(last_rides) > 0:
                        ride_id = last_rides[0][0]
                        div = graphs(data_base, ride_id, car_id)
                        content = gzip.compress(json.dumps({'div': div}).encode('utf8'), 5)
                        response = make_response(content)
                        response.headers['Content-length'] = len(content)
                        response.headers['Content-Encoding'] = 'gzip'
                        return response
                  else:
                        return "None"
            else:
                  abort(401)
      except Exception as e:
            print(e)
            return "None"

@api.route('/api/download/database', methods=['GET'])
def download_db():
      try:
            if session['usr_id']:
                  data_base = DataBase()
                  user_id, user_name, user_email = data_base.getUserInfo(session['usr_id'])[0]
                  if user_email in adminList:
                        try:
                              return send_file("data/database.db",as_attachment = True)
                        except FileNotFoundError:
                              abort(404)
                        except Exception as e:
                              abort(500)
                  data_base.close()
            else: 
                  abort(401)
      except Exception as e:
            print(e)
            return redirect(url_for('login'))

@api.route('/api/signin', methods=['POST'])
def signin():
      try:
            form = request.form
            email = form['email']
            password = form['password']
            user = auth.sign_in_with_email_and_password(email, password)
            user = auth.refresh(user['refreshToken'])
            session['usr_id'] = user['userId']
            session['usr_token'] = user['idToken']
            return redirect(url_for('index'))
      except Exception as e:
            print(e)
            #return render_template("login.html", message=("login","Incorrect email/password combination."), login=True)
            return redirect(url_for("login", failed=json.dumps({'login': True})))

@api.route('/api/signup', methods=['POST'])
def signup():
      try:
            form = request.form
            email = form['email']
            password = form['password']
            name = form['name']
            auth.create_user_with_email_and_password(email, password)
            user = auth.sign_in_with_email_and_password(email, password)
            user = auth.refresh(user['refreshToken'])
            session['usr_id'] = user['userId']
            session['usr_token'] = user['idToken']
            db = DataBase()
            db.createUser(user['userId'], name, email)
            db.close()
            return redirect(url_for('index'))
      except Exception as e:
            print(e)
            return redirect(url_for("login", failed=json.dumps({'login': False})))

@api.route('/api/register', methods=['POST'])
def register():
      try:
            data = request.json
            db = DataBase()
            db.createUser(data['user_id'],data['name'],data['email'])
            db.close()
            return jsonify({"message": "OK: Authorized"}), 200
      except:
            return jsonify({"message": "ERROR: Bad Request"}), 400

@api.route('/api/push', methods=['POST'])
def push():
      try:
            data = request.json
            db = DataBase()
            db.addData(data['data'], data['user_id'], data['time'])
            db.close()
            return jsonify({"message": "OK: Authorized"}), 200
      except Exception as e:
            print(e)
            return jsonify({"message": "ERROR: Bad Request"}), 400

@api.errorhandler(Exception)
def page_not_found(e):
      try:
            code = str(e.code)
      except:
            code = ""

      try:      
            name = e.name
      except:
            name = ""

      try:
            description = e.description
      except:
            description = ""

      return render_template("error.html",error_name=code + " " + name, error_description=description)

api.run(host = '127.0.0.1', port = 8080)
