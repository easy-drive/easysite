from data.database import *


def derivate(array):
    derivative = []
    for i in range(1, len(array) - 1):
        t = (array[i][0] + array[i-1][0])/2
        d = (array[i][1] - array[i-1][1])/(array[i][0] - array[i-1][0])
        derivative.append((t, d))
    return derivative


def integrate(array):
    integral = 0
    for i in range(1, len(array)):
        integral += (array[i-1][1] + array[i][1]) * \
            (array[i][0] - array[i-1][0])/2
    return integral


def average(array):
    return integrate(array) / (array[-1][0] - array[0][0])


def average_ride_speed(ride_id, car_id):
    db = DataBase()
    speed = db.getSpeed(ride_id, car_id)
    return average(speed)


def average_ride_acceleration(ride_id, car_id):
    db = DataBase()
    speed = db.getSpeed(ride_id, car_id)
    acceleration = derivate(speed)
    return average(acceleration)*(3600000**2)


def travelled_distance(ride_id, car_id):
    db = DataBase()
    times = db.getTimeMinTimeMax(ride_id, car_id)
    average_speed = average_ride_speed(ride_id, car_id)
    time_min_h = (times[0][0])/(3600000)
    time_max_h = (times[0][1])/(3600000)
    return average_speed*(time_max_h-time_min_h)


if __name__ == '__main__':
    print("Average speed :", average_ride_speed(0, "MAT403096BNL00000"), "km/h")
    print("Average acceleration :",
          average_ride_acceleration(0, "MAT403096BNL00000"), "km/h2")
    print("Distance parcourure :", travelled_distance(
        0, "MAT403096BNL00000"), "km")
