import sqlite3
from database import *
from analyse_données import *
import random
import statistics

db = DataBase()


def initialise(ride_id, car_id):
    #time, v, rpm, afor, alat, fuel
    time = db.getTimeRide(ride_id, car_id)
    v = [e[1] for e in db.getSpeed(ride_id, car_id)]
    v.reverse()
    rpm = [e[0] for e in db.getRPM(ride_id, car_id)]
    rpm.reverse()
    afor = [e[1] for e in derivate(db.getSpeed(ride_id, car_id))]
    afor.reverse()
    alat = [random.uniform(0, 1) for e in afor]
    fuel = [e[0] for e in db.getFuelConsumption(ride_id, car_id)]
    fuel.reverse()
    return time, v, rpm, afor, alat, fuel


def driven_time(time):
    limittime = 7200
    counter_t = 0
    for i in range(2, len(time)):
        if time[i] >= limittime and time[i-1] < limittime:
            counter_t = 1
    # if counter_t == 1:
    #    return "Too much time driven"
    # else:
    #    return "properly driven time"
    return counter_t


limitfuel = 15


def fuel_consumption(fuel):
    counter_fuel = 0
    limitfuel_inf = 10
    if statistics.mean(fuel) >= limitfuel_inf:
        for i in range(2, len(fuel)):
            if (fuel[i] >= limitfuel) and (fuel[i-1] < limitfuel):
                counter_fuel += 1
            if (fuel[i] <= limitfuel) and (fuel[i-1] > limitfuel):
                counter_fuel -= 1
            if counter_fuel <= 0:
                counter_fuel = 0
    if statistics.mean(fuel) <= limitfuel_inf:
        counter_fuel = 0
    return counter_fuel


def acceleration_counter(afor):
    limitafor_pos = 0.5
    counter_aforpos = 0
    for i in range(2, len(afor)):
        if afor[i] >= limitafor_pos and afor[i-1] < limitafor_pos:
            counter_aforpos += 1
    return counter_aforpos


def engine_rotation(rpm):
    limitrpm = 2000
    counter_rpm = 0
    for i in range(2, len(rpm)):
        if rpm[i] >= limitrpm and rpm[i-1] < limitrpm:
            counter_rpm += 1
    return counter_rpm


def eff_marks(afor, fuel, time, rpm):
    drive_t = max(time)
    a_mark = 2-40*(acceleration_counter(afor)/drive_t)
    fuel_mark = 5-180*(fuel_consumption(fuel)/drive_t)
    rpm_mark = 3-30*(engine_rotation(rpm)/drive_t)
    return a_mark, fuel_mark, rpm_mark


def efficiency(afor, fuel, time, rpm):
    t = max(time)
    counter_afor = acceleration_counter(afor)
    counter_rpm = engine_rotation(rpm)
    counter_fuel = fuel_consumption(fuel)
    if statistics.mean(fuel) >= limitfuel:
        eff = 5-40*(counter_afor/t)-30*(counter_rpm/t)
    else:
        eff = 10-40*(counter_afor/t)-180*(counter_fuel/t)-30*(counter_rpm/t)
    return eff


def lateral_acc(alat):
    limitalat = 0.4
    counter_alat = 0
    limitalatneg = -0.4
    for i in range(2, len(alat)):
        if alat[i] >= limitalat and alat[i-1] < limitalat:
            counter_alat += 1
        if alat[i] <= limitalatneg and alat[i-1] > limitalatneg:
            counter_alat += 1
    return counter_alat


def dangerous_acc(afor):
    limitdangafor = -0.4
    counter_dang_afor = 0
    for i in range(2, len(afor)):
        if afor[i] <= limitdangafor and afor[i-1] > limitdangafor:
            counter_dang_afor += 1
    return counter_dang_afor


def negative_acc(afor):
    limitnegafor = -0.3
    counter_neg_afor = 0
    for i in range(2, len(afor)):
        if afor[i] <= limitnegafor and afor[i-1] > limitnegafor:
            counter_neg_afor += 1
    return counter_neg_afor


def speed_counter(v):
    limitv = 51
    counter_v = 0
    for i in range(2, len(v)):
        if v[i] >= limitv and v[i-1] < limitv:
            counter_v += 1
    return counter_v


def sec_marks(alat, afor, time, v):
    t = max(time)
    alat_mark = 2-30*(lateral_acc(alat)/t)
    dang_acc_mark = 3-200*(dangerous_acc(afor)/t)
    nag_acc_mark = 2-50*(negative_acc(afor)/t)
    v_mark = 3-80*(speed_counter(v)/t)
    return alat_mark, dang_acc_mark, nag_acc_mark, v_mark


def security(alat, afor, time, v):
    marks = sec_marks(alat, afor, time, v)
    sec = sum(marks)
    return sec if driven_time(time) == 0 else sec-1


def driving_mark(ride_id, car_id):
    vec = initialise(ride_id, car_id)
    eff = efficiency(vec[3], vec[5], vec[0], vec[2])
    sec = security(vec[4], vec[3], vec[0], vec[1])
    return (eff+sec)/2


def evaluation(ride_id, car_id):
    mark = driving_mark(ride_id, car_id)
    if mark <= 2:
        return "Very bad driving. Driver needs to improve immediately!"
    elif 2 < mark <= 4:
        return "Bad driving. Driver must improve."
    elif 4 < mark <= 6:
        return "Regular driving, however it could be improved."
    elif 6 < mark <= 8:
        return "Good driving, although it could be slightly improved."
    else:
        return "Excellent driving!"


if __name__ == '__main__':
    time, v, rpm, afor, alat, fuel = initialise(0, "MAT403096BNL00000")
    print(eff_marks(afor, fuel, time, rpm))
    print(efficiency(afor, fuel, time, rpm))
    print(sec_marks(alat, afor, time, v))
    print(security(alat, afor, time, v))
    print(driving_mark(0, "MAT403096BNL00000"))
    print(evaluation(0, "MAT403096BNL00000"))
