import pandas as pd
from bs4 import BeautifulSoup as bs
import urllib.request

url = 'https://www.carburants.org/'
page = urllib.request.urlopen(url, timeout=5)
soup = bs(page, features="lxml")

# on récupère les données du site
gas = soup.find_all('span', {'class': 'text-justify d-block p-1'})

# création du dictionnaire qui contient le nom du carburant et son prix au L en €
prix_carburant = {}
for e in gas:
    e = e.text
    e = e.replace('<strong >', '')
    e = e.replace(',', '.')
    poscarbu1 = e.find('Le')
    poscarbu2 = e.find('est')
    posprix1 = e.find('actuellement')
    posprix2 = e.find('€')
    # on extrait les prix de la chaîne de caractères
    prix_carburant[e[poscarbu1+3: poscarbu2 - 1]] = e[posprix1+16:posprix2-1]

print(prix_carburant)  # dictionnaire complet
