from os import times
from database import *
from prix_carburant import *

# https://lcsi.umh.es/docs/pfcs/PFC_TFG_Bocanegra_Fernando.pdf

# hard coded values meant to be replaced
TIME_LIMIT = 7200       # en secondes
FUEL_LIMIT_SUP = 15     # en litres par 100km
FUEL_LIMIT_INF = 10     # en litres par 100km
FUEL_CAPACITY = 50      # en litres


def derivate(array):
    derivative = []
    for i in range(1, len(array) - 1):
        t = (array[i+1][0] + array[i-1][0])/2
        d = (array[i+1][1] - array[i-1][1])/(array[i+1][0] - array[i-1][0])
        derivative.append((t, d))
    return derivative


def integrate(array):
    integral = 0
    for i in range(1, len(array)):
        integral += (array[i-1][1] + array[i][1]) * \
            (array[i][0] - array[i-1][0])/2
    return integral


def average(array):
    return integrate(array) / (array[-1][0] - array[0][0])


def averageRideSpeed(ride_id, car_id):
    db = DataBase()
    speed = db.getSpeed(ride_id, car_id)
    return average(speed)


def averageRideAcceleration(ride_id, car_id):
    db = DataBase()
    speed = db.getSpeed(ride_id, car_id)
    acceleration = derivate(speed)
    return average(acceleration)


def travelDuration(ride_id, car_id):
    db = DataBase()
    times = db.getTimeMinTimeMax(ride_id, car_id)
    return (times[0][1] - times[0][0]) / 1000


def travelledDistance(ride_id, car_id):
    average_speed = averageRideSpeed(ride_id, car_id)
    duration = travelDuration(ride_id, car_id)
    return average_speed * duration


def exceedingTravelDuration(ride_id, car_id):
    return max(0, travelDuration() - TIME_LIMIT)

# Ne serait-il pas intéressant de calculer la durée de dépassement
# de la consommation moyenne plutôt que d'avoir un compteur qui considère
# indifféremment un dépassement qui dure 1h et un dépassement qui dure 1 seconde ?


def calculateFuelCounter(ride_id, car_id):
    db = DataBase()
    fuel_level = db.getFuelLevel(ride_id, car_id)
    speed = db.getSpeed(ride_id, car_id)

    counter = 0
    # la valeur n'est pas très importante
    previous_fuel_consumption = (FUEL_LIMIT_INF + FUEL_LIMIT_SUP) / 2
    # il suffit qu'elle soit entre FUEL_LIMIT_INF et FUEL_LIMIT_SUP

    for i in range(1, len(fuel_level)):
        time_step = speed[i][0] - speed[i-1][0]
        distance_step = speed[i][1] * time_step
        fuel_consumption = (
            fuel_level[i][0] - fuel_level[i-1][0]) * FUEL_CAPACITY / distance_step

        if fuel_consumption >= FUEL_LIMIT_SUP and previous_fuel_consumption < FUEL_LIMIT_SUP:
            counter += 1
        elif fuel_consumption <= FUEL_LIMIT_INF and previous_fuel_consumption > FUEL_LIMIT_INF:
            counter -= 1

        # dans l'algorithme initial cette condition est placée ici (dans la boucle)
        # quel est l'intéret de mettre ça ici plutôt qu'en fin de boucle ?
        if counter < 0:
            counter = 0

        previous_fuel_consumption = fuel_consumption

    return counter


def travelCost(ride_id, car_id):
    db = DataBase()
    times = db.getTimeMinTimeMax(ride_id, car_id)
    initial_fuel_level = (db.getFuelLevel(ride_id, car_id, times[0][0]))[0][0]
    final_fuel_level = (db.getFuelLevel(ride_id, car_id, times[0][1]))[0][0]
    consumed_fuel = (final_fuel_level-initial_fuel_level)*FUEL_CAPACITY
    cost = consumed_fuel*float(prix_carburant['SP95'])
    return cost


if __name__ == '__main__':
    print("Average speed :", averageRideSpeed(0, "MAT403096BNL00000"), "km/h")
    print("Average acceleration :",
          averageRideAcceleration(0, "MAT403096BNL00000"), "km/h2")
    print("Distance parcourure :", travelledDistance(
        0, "MAT403096BNL00000"), "km")
    print("Travel duration :", travelDuration(0, "MAT403096BNL00000"))
    print("Travel cost :", travelCost(0, "MAT403096BNL00000"), "€")
