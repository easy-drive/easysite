from database import *
from analyse_données import *
from Notation import *
import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.offline
from plotly.subplots import make_subplots
import pandas as pd
from datetime import datetime


def dashboard(ride_id, car_id):
    fig = make_subplots(
        rows=3, cols=6,
        specs=[
            [{"type": "indicator", "colspan": 1}, {"type": "indicator",
                                                   "colspan": 1}, {"type": "indicator", "colspan": 1}, {
                "type": "bar", "colspan": 3, "rowspan": 2}, None, None],
            [{"type": "scatter", "colspan": 3}, None, None, None,  None, None],
            [{"type": "scatter", "colspan": 3}, None, None, {
                "type": "indicator", "colspan": 1}, {
                "type": "indicator", "colspan": 1}, {
                "type": "indicator", "colspan": 1}]
        ]
    )

    fig.add_trace(
        go.Indicator(
            mode="number",
            value=averageRideSpeed(ride_id, car_id),
            title="Average Speed",
        ),
        row=1, col=1
    )

    fig.add_trace(
        go.Indicator(
            mode="number",
            value=travelCost(ride_id, car_id),
            title="Travel Cost",
        ),
        row=1, col=2
    )

    fig.add_trace(
        go.Indicator(
            mode="number",
            value=travelledDistance(ride_id, car_id),
            title="Total Distance per ride",
        ),
        row=1, col=3
    )

    fig.add_trace(
        go.Scatter(
            x=plotSpeed(ride_id, car_id)[0],
            y=plotSpeed(ride_id, car_id)[1],
            name="Speed",
            marker=dict(color="Yellow"),
            showlegend=True,
        ),
        row=2, col=1
    )

    fig.add_trace(
        go.Scatter(
            x=plotAcceleration(ride_id, car_id)[0],
            y=plotAcceleration(ride_id, car_id)[1],
            name="Acceleration",
            marker=dict(color="Green")
        ),
        row=3, col=1
    )

    fig.add_trace(
        go.Scatter(
            x=plotAcceleration(ride_id, car_id)[0],
            y=[51 for e in plotAcceleration(ride_id, car_id)[0]],
            marker=dict(color="Orange"),
            name="Speed Limit",
        ),
        row=2, col=1
    )

    fig.add_trace(
        go.Bar(
            name='1',
            x=['ecodriving', 'security'],
            y=[summary(ride_id, car_id)[0][0], summary(ride_id, car_id)[2][1]],
            marker_color=['yellow', 'orange'],
            showlegend=False,
            text=['Acceleration mark', 'Dangerous acc mark']
        ),
        row=1, col=4
    )

    fig.add_trace(
        go.Bar(
            name='2',
            x=['ecodriving', 'security'],
            y=[summary(ride_id, car_id)[0][1], summary(ride_id, car_id)[2][2]],
            marker_color=['lightgreen', 'green'],
            showlegend=False,
            text=['Fuel consumption mark', 'Negative acc mark']
        ),
        row=1, col=4
    )

    fig.add_trace(
        go.Bar(
            name='3',
            x=['ecodriving', 'security'],
            y=[summary(ride_id, car_id)[0][2], summary(ride_id, car_id)[2][3]],
            marker_color=['pink', 'crimson'],
            showlegend=False,
            text=['Rpm mark', 'Speed mark']
        ),
        row=1, col=4
    )

    fig.add_trace(
        go.Indicator(
            mode="number",
            value=summary(ride_id, car_id)[-2],
            title=summary(ride_id, car_id)[-1],
        ),
        row=3, col=6
    )
    fig.add_trace(
        go.Indicator(
            mode="number",
            value=summary(ride_id, car_id)[1],
            title="EcoDriving Mark/10",
        ),
        row=3, col=4
    )
    fig.add_trace(
        go.Indicator(
            mode="number",
            value=summary(ride_id, car_id)[3],
            title="Security Mark/10",
        ),
        row=3, col=5
    )

    fig.update_layout(
        template="plotly_white",
        title={
            'text': "Driving Dashboard",
            'y': 0.95,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},
        showlegend=True,
        legend_orientation="h",
        legend=dict(x=0.2, y=0.7),
    )

    print(plotly.offline.plot(fig, include_plotlyjs=False, output_type='div'))
    return "dashboard.html"


def plotSpeed(ride_id, car_id):
    db = DataBase()
    tuples = db.getSpeed(ride_id, car_id)
    x, y = [], []
    for e in tuples[len(tuples)-20:len(tuples)]:
        dt = datetime.fromtimestamp(e[0]/1000)
        x += [dt.strftime("%Y-%m-%d %H:%M:%S")]
        y += [e[1]]
    # fig, ax = plt.subplots()
    # plt.plot(x, y)
    # plt.xticks(rotation=45)
    # fig.savefig('./static/images/speed.png', bbox_inches='tight')
    return x, y


def plotAcceleration(ride_id, car_id):
    db = DataBase()
    tuples = db.getSpeed(ride_id, car_id)
    tuplesA = derivate(tuples)
    x, y = [], []
    for e in tuplesA[len(tuplesA)-20:len(tuplesA)]:
        dt = datetime.fromtimestamp(e[0]/1000)
        x += [dt.strftime("%Y-%m-%d %H:%M:%S")]
        y += [e[1]]
    # fig, ax = plt.subplots()
    # plt.plot(x, y)
    # plt.xticks(rotation=45)
    # fig.savefig('./static/images/acceleration.png',bbox_inches='tight')
    return x, y


# def plotAvgSpeed(ride_id, car_id):
    # data
    label = ["A", "B", "C"]
    val = [1, 2, 3]

    # append data and assign color
    label.append("")
    val.append(sum(val))  # 50% blank
    colors = ['red', 'blue', 'green', 'white']

    # plot
    fig = plt.figure(figsize=(8, 6), dpi=100)
    ax = fig.add_subplot(1, 1, 1)
    ax.pie(val, labels=label, colors=colors)
    ax.add_artist(plt.Circle((0, 0), 0.6, color='white'))
    plt.show()


if __name__ == '__main__':
    # plotSpeed(0, "MAT403096BNL00000")
    # plotAcceleration(0, "MAT403096BNL00000")
    # plotAvgSpeed(0, "MAT403096BNL00000")
    dashboard(0, "MAT403096BNL00000")
