let active_table = "raw_driving_data";
let table;

$(document).ready(function () {
  postRequest(active_table);
});

$("#refresh").click(() => {
  postRequest(active_table);
});

$("#process").click(() => {
  processRequest(active_table);
});

for (let i=0; i < tables.length; i++) {
  $(`#${tables[i]}`).click(() => {
    postRequest(tables[i]);
  });
}

const updateTable = (data) => {;
  let header  = [];
  let body = data.data;
  for (col in data.columns) {
    header.push({ "title": data.columns[col], "autoWidth": true, "class":"read_only" });
  }
  if (table) {
    table.destroy();
    $("#db_table").empty();
  }
  table = $("#db_table").DataTable(options(header,body));
  $("thead").addClass("thead-dark");
};

const updateFail = () => {
  alert("Refresh failed!");
}

const postRequest = (table_name) => {
  $.ajax ({
    url: "http://easydrive.cs-campus.fr/api/get_database",
    type: "POST",
    data: JSON.stringify({table:table_name}),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: (data, status) => {
      if (data == "None") {
        updateFail();
      } else {
          $("#"+active_table).removeClass('active');
          active_table = table_name;
          $("#"+active_table).addClass('active');
          updateTable(data);
      }
    },
    error: () => {
      updateFail();
    }
  });
}

const processRequest = (table_name) => {
  $.ajax ({
    url: "http://easydrive.cs-campus.fr/api/process",
    type: "POST",
    data: JSON.stringify({table:table_name}),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: (data, status) => {
      if (data == "None") {
        updateFail();
      } else {
          $("#"+active_table).removeClass('active');
          active_table = table_name;
          $("#"+active_table).addClass('active');
          updateTable(data);
      }
    },
    error: () => {
      updateFail();
    }
  });
}

const options = (header,body) => {
  let args = {
    destroy: true,
    columns: header,
    data: body,
    "deferRender": true,
    "pagingType": "full_numbers",
    "pageLength": 25
  };
  if (active_table == "raw_driving_data") {
    args["order"] = [1,"desc"];
  }
  return args
}