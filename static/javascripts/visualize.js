$(document).ready(function () {
    getNumbers();
});

let state = "numbers";
let cards = $("#cards");
let container = $("#container");
let previous_button = $("#previous_button");
let refresh = $("#refresh");
let table;

refresh.click(() => {
    if (state == "numbers") {
        getNumbers();
    }
})

const getNumbers = () => {
  $.ajax ({
    url: "http://easydrive.cs-campus.fr/api/get_data",
    type: "POST",
    data: JSON.stringify({operation:"getNumbers"}),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: (data, status) => {
      if (data == "None") {
        updateFail();
      } else {
          updateNumbers(data);
      }
    },
    error: () => {
      updateFail();
    }
  });
};

const launchContent = (func) => {
    $.ajax ({
        url: "http://easydrive.cs-campus.fr/api/get_data",
        type: "POST",
        data: JSON.stringify({operation:func}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status) => {
            if (data == "None") {
                launchFailed();
            } else {
                updateContainer(func,data);
            }
        },
        error: () => {
            launchFailed();
        }
    });
}

const launchUserProfile = (func,user_id,user_name) => {
    $.ajax ({
        url: "http://easydrive.cs-campus.fr/api/get_data",
        type: "POST",
        data: JSON.stringify({operation:"UserProfile", user_id:user_id}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status) => {
            if (data == "None") {
                userProfileLaunchFailed();
            } else {
                renderUserProfile(func,data, user_id, user_name);
            }
        },
        error: () => {
            userProfileLaunchFailed();
        }
    });
}

const launchCarProfile = (func,car_id,from_user=false, user_id='', user_name='') => {
        $.ajax ({
        url: "http://easydrive.cs-campus.fr/api/get_data",
        type: "POST",
        data: JSON.stringify({operation:"CarProfile", car_id:car_id}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status) => {
            if (data == "None") {
                carProfileLaunchFailed();
            } else {
                renderCarProfile(func,data, car_id, from_user, user_id, user_name);
            }
        },
        error: () => {
            carProfileLaunchFailed();
        }
    });
}

const launchRideProfile = (func, ride_id, car_id,from_user=false, user_id='', user_name='') => {
        $.ajax ({
        url: "http://easydrive.cs-campus.fr/api/get_data",
        type: "POST",
        data: JSON.stringify({operation:"RideProfile", ride_id:ride_id, car_id:car_id}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status) => {
            if (data == "None") {
                rideProfileLaunchFailed();
            } else {
                renderRideProfile(func,data, ride_id, car_id, from_user, user_id, user_name);
            }
        },
        error: () => {
            rideProfileLaunchFailed();
        }
    });
}

const renderUsers = (data,func) => {
    displayTable(data);
    table.on('click', 'tbody tr', function() {
        launchUserProfile(func,table.row(this).data()[0],table.row(this).data()[1]);
    });
}

const renderCars = (data,func) => {
    displayTable(data);
    table.on('click', 'tbody tr', function() {
        launchCarProfile(func,table.row(this).data()[0]);
    });
}

const renderRides = (data) => {
    displayTable(data,[2, "desc"]);
}

const renderUserProfile = (func, data, user_id, user_name) => {
    state = "UserProfile";
    if (table) {
        table.destroy();
        $("db_table").remove();
    }
    container.html("<table id='db_table' class='table table-hover table-bordered'></table>");
    $("#page_title").text(`User Profile : ${user_name}`);
    previous_button.off("click");
    previous_button.click(() => {
        state = func;
        $("#page_title").text(func);
        launchContent(func);
    });
    displayTable(data);
    table.on('click', 'tbody tr', function() {
        launchCarProfile(func,table.row(this).data()[0],true,user_id,user_name);
    });
    refresh.off("click");
    refresh.click(() => {
        launchUserProfile(func,user_id,user_name);
    });
}

const renderCarProfile = (func, data, car_id, from_user=false, user_id='', user_name='') => {
    state = "CarProfile";
    if (table) {
        table.destroy();
        $("db_table").remove();
    }
    container.html("<table id='db_table' class='table table-hover table-bordered'></table>");
    $("#page_title").text(`Car Profile : ${car_id}`);
    previous_button.off("click");
    previous_button.click(() => {
        if (from_user) {
            launchUserProfile(func,user_id,user_name);
        } else {
        state = func;
        $("#page_title").text(func);
        launchContent(func);
        }
    });
    displayTable(data);
    refresh.off("click");
    refresh.click(() => {
        launchCarProfile(func, car_id,from_user,user_id,user_name);
    });
    table.on('click', 'tbody tr', function() {
        launchRideProfile(func, table.row(this).data()[0], car_id, from_user, user_id, user_name);
    });
}

const renderRideProfile = (func, data, ride_id, car_id, from_user=false, user_id='', user_name='') => {
    state = "RideProfile";
    if (table) {
        table.destroy();
        $("db_table").remove();
    }
    container.html("<table id='db_table' class='table table-hover table-bordered'></table>");
    $("#page_title").text(`Ride Profile : Ride Number ${ride_id} of ${car_id}`);
    previous_button.off("click");
    previous_button.click(() => {
        state = func;
        $("#page_title").text(func);
        launchCarProfile(func, car_id, from_user, user_id, user_name);
    });
    displayTable(data);
    refresh.off("click");
    refresh.click(() => {
        launchRideProfile(func, ride_id, car_id, from_user, user_id, user_name);
    });
}

const updateNumbers = (data) => {
    let cards_html = "";
    let ids = [];
    for (let i = 0; i < data.length; i++) {
        cards_html += `<div id="${data[i]["title"]}" class="col-4">
                            <div class="card text-center mt-3">
                                <div class="card-header pt-5 pb-5 font-weight-bold">
                                    ${data[i]["number"]}
                                </div>
                                <div class="card-footer pt-4 pb-4 font-weight-bold">
                                    ${data[i]["title"]}
                                </div>
                            </div>
                        </div>`;
        ids.push(data[i]["title"]);
    }
    cards.html(cards_html);
    for (let i = 0; i < ids.length; i++) {
        $(`#${ids[i]}`).click(() => {
            launchContent(ids[i]);
        })
    }
}

const updateContainer = (func,data) => {
    state = func;
    cards.hide();
    previous_button.show();
    $("#page_title").text(func);
    previous_button.off("click");
    previous_button.click(() => {
        state = "numbers";
        if (table) {
            table.destroy();
            $("db_table").remove();
        }
        container.hide();
        cards.show();
        previous_button.hide();
        $("#page_title").text("EasyDrive Visualized");
        refresh.off("click");
        refresh.click(() => {
            if (state == "numbers") {
                getNumbers();
            }
        });
    });
    container.html("<table id='db_table' class='table table-hover table-bordered'></table>");
    container.show();
    switch (func) {
        case "Users":
            renderUsers(data,func);
            break;
        case "Cars":
            renderCars(data,func);
            break;
        case "Rides":
            renderRides(data);
            break;
        default:
            break;
    }
    refresh.off("click");
    refresh.click(() => {
        launchContent(func);
    });
}

const launchFailed = () => {
    alert("Launch failed!");
}

const updateFail = () => {
    alert("Refresh failed!");
}

const userProfileLaunchFailed = () => {
    alert("User Profile Launch failed!");
}

const carProfileLaunchFailed = () => {
    alert("Car Profile Launch failed!");
}

const rideProfileLaunchFailed = () => {
    alert("Ride Profile Launch failed!");
}

const displayTable = (data, order = false) => {
    let header  = [];
    let body = data.data;
    for (col in data.columns) {
        header.push({ "title": data.columns[col], "autoWidth": true, "class":"read_only" });
    }
    let args = {
        columns: header,
        data: body,
        "deferRender": true,
        "pagingType": "full_numbers",
        "pageLength": 10
    };
    if (order) {
        args["order"] = order;
    }
    table = $("#db_table").DataTable(args);
    $("thead").addClass("thead-dark");
}