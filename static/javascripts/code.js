$(document).ready(() => {
    $('html, body').animate({
        scrollTop: $('.easy-logo').offset().top
    }, 'slow');
});
let editor;
editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.session.setMode("ace/mode/python");
editor.renderer.setScrollMargin(20, 20);

let consolePrint;
consolePrint = $('#console');
$('#run').on('click', () => {
    let code = editor.getValue();
    if (code) {
        runCode(code);
    }
});

$('#clear').on('click', () => {
    clearConsole();
});

$('#refresh').on('click', () => {
    $(document).ready(() => {
        $('html, body').animate({
            scrollTop: $('.easy-logo').offset().top
        }, 'slow');
    });
    editor = ace.edit("editor");
    editor.setTheme("ace/theme/monokai");
    editor.session.setMode("ace/mode/python");
    editor.renderer.setScrollMargin(20, 20);
    consolePrint = $('#console');
    clearConsole();
});

const runCode = (code) => {
    $.ajax ({
        url: "http://easydrive.cs-campus.fr/api/run_code",
        type: "POST",
        data: JSON.stringify({code:code}),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: (data, status) => {
            printResponse(data.response);
        },
        error: (e) => {
        printResponse(e);
        }
    });
}

const printResponse = (response) => {
    consolePrint.append("<br>>>> " + response.replaceAll("\n","<br>"));
}

const clearConsole = () => {
    consolePrint.html('');
}