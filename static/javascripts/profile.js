$(document).ready(function () {
  postRequest(userId);
});

let holder = $("#holder");

const postRequest = (userId) => {
  $.ajax ({
    url: "http://easydrive.cs-campus.fr/api/dashboard",
    type: "POST",
    data: JSON.stringify({user_id:userId}),
    dataType: "json",
    contentType: "application/json; charset=utf-8",
    success: (data, status) => {
      if (data == "None") {
        loadingFailed();
      } else {
        console.log(data.div);
        holder.html(data.div);
      }
    },
    error: (e) => {
        loadingFailed();
    }
  });
};

const loadingFailed = () => {
  holder.html(`<h1 class="title">Welcome, ${name}! </h1>`);
  alert("Loading failed!");
}