from database import DataBase

def run(data):
    try:
        db = DataBase()
        db.addCar(data['car_id'],data['user_id'],data['time'])
        db.close()
    except Exception as e:
        raise Exception(e)