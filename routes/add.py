from database import DataBase

def run(data):
    try:
        db = DataBase()
        db.addData(data['data'])
        db.close()
    except Exception as e:
        raise Exception(e)