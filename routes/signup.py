from database import DataBase

def run(data):
    try:
        db = DataBase()
        db.createUser(data['user_id'],data['name'],data['email'])
        db.createCar(data['car_id'],data['user_id'],data['time'])
        db.close()
    except Exception as e:
        raise Exception(e)