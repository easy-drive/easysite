import sqlite3
from constants import *
from analyse import *
from notation import *

data_columns = ['car_id','time','latitude','longitude','RPM','SPEED','FUELLEVEL']
possible_columns = ['car_id','time','latitude','longitude','altitude','RPM','SPEED','AMBIENTTEMPERATURE','THROTTLEPOSITION','ABSOLUTELOAD','FUELLEVEL']

processed_data_columns = ['car_id', 'ride_id', 'AverageSpeed', 'RideDistance','RideCost', 'Efficiency', 'Security', 'DrivingMark']
possible_processed_columns = ['car_id','ride_id','AverageSpeed','AverageAcceleration','RideDistance','RideTime','RideCost','FuelConsumption','AccelerationCounter','EngineROtation','Efficiency','LateralAcceleration','DangerousAcceleration','NegativeAcceleration','SpeedCounter','Security','DrivingMark']


class DataBase:

    def __init__(self, filepath = 'data/database.db', check_same_thread = True, verbose = False):
        try:
            self.con = sqlite3.connect(filepath)
            self.cur = self.con.cursor()
            self.verbose = verbose
        except Exception as e:
            raise Exception(e)

    def __del__(self):
        try:
            self.con.close()
        except Exception as e:
            raise Exception(e)
    
    def execute(self, request):
        try:    
            self.cur.execute(request)
            self.con.commit()
        except Exception as e:
            raise Exception(e)

    def setVerboseState(state):
        self.verbose = state 

    def createDB(self):
        try:
            print('Creating users DB...')
            self.execute('''CREATE TABLE users (user_id TEXT PRIMARY KEY NOT NULL, name TEXT NOT NULL, email TEXT NOT NULL);''')
            print('Created users DB')
        except:
            print('Cannot create users DB')
   
        try:
            # car_id is VIN
            print('Creating cars DB...')
            self.execute('''CREATE TABLE cars (car_id TEXT NOT NULL, user_id, time timestamp NOT NULL, PRIMARY KEY (car_id,user_id,time),FOREIGN KEY (user_id) REFERENCES users(user_id));''')
            print('Created cars DB')
        except:
            print('Cannot create cars DB')
                   
        try:
            print('Creating raw_driving_data DB...')
            self.execute('''CREATE TABLE raw_driving_data (car_id, time timestamp NOT NULL, latitude FLOAT, longitude FLOAT, altitude FLOAT, RPM FLOAT, SPEED FLOAT, AMBIENTAIRTEMPERATURE FLOAT, THROTTLEPOSITION FLOAT, ABSOLUTELOAD FLOAT, FUELLEVEL FLOAT,PRIMARY KEY (car_id,time),FOREIGN KEY (car_id) REFERENCES cars(car_id));''')
            print('Created raw_driving_data DB')
        except:
            print('Cannot create raw_driving_data')       

        try:
            print('Creating rides DB...')
            self.execute('''CREATE TABLE rides(car_id, ride_id, time, PRIMARY KEY (car_id, time), FOREIGN KEY (car_id, time) REFERENCES raw_driving_data(car_id, time));''')
            print('Created rides DB')  
        except:
            print('Cannot create rides DB')    

        try:
            print('Creating processed_data DB...')
            self.execute('''CREATE TABLE processed_data(car_id, ride_id, AverageSpeed FLOAT, AverageAcceleration FLOAT, RideDistance FLOAT, RideTime FLOAT, RideCost FLOAT, FuelConsumtion FLOAT, AccelerationCounter FLOAT, EngineRotation FLOAT, Efficiency FLOAT, LateralAcceleration FLOAT, DangerousAcceleration FLOAT, NegativeAcceleration FLOAT, SpeedCounter FLOAT, Security FLOAT, DrivingMark FLOAT, PRIMARY KEY (car_id, ride_id), FOREIGN KEY (car_id, ride_id) REFERENCES rides(car_id, ride_id));''')
            print('Created processed_data DB')
        except:
            print('Cannot create processed_data DB')               

    def createRides(self):
        try:
            CUT_OFF_PERIOD = 1000000
            cars = self.getCarsOnly()
            rides = []
            for car in cars:
                car_data = self.getCarData(car[0])
                car_data.sort(key=lambda x: x[1])
                last = car_data[0][1]
                car_rides = []
                ride_id = 0
                ride = [[car_data[0][0], car_data[0][1]]]
                try:
                    self.createRide(ride_id, car_data[0][0], car_data[0][1])
                except:
                    pass
                for row in car_data[1:]:
                    if row[1] - last > CUT_OFF_PERIOD:
                        car_rides.append(ride)
                        ride = [[row[0], row[1]]]
                        ride_id += 1
                        try:
                            self.createRide(ride_id,row[0],row[1])
                        except:
                            pass
                    else:
                        ride.append([row[0], row[1]])
                        try:
                            self.createRide(ride_id,row[0], row[1])
                        except:
                            pass
                    last = row[1]
                car_rides.append(ride)
                rides.append(car_rides)
        except Exception as e:
            pass
    
    def createUser(self, user_id, name, email):
        try:
            self.execute(f'''INSERT INTO users VALUES ("{user_id}","{name}","{email}");''')
            return True
        except Exception as e:
            raise Exception(e)

    def createCar(self, car_id, user_id, time):
        try:
            if user_id != 'null':
                self.execute(f'''INSERT INTO cars VALUES ("{car_id}","{user_id}", "{time}");''')
                return True
            raise Exception("user_id NOT VALID")
        except Exception as e:
            raise Exception(e)

    def createCarModel(self, model_id,model_name):
        try:
            self.execute(
                f'''INSERT INTO car_models VALUES ("{model_id}","{model_name}");''')
            return True
        except Exception as e:
            raise Exception(e)

    def createRawDrivingData(self, car_id, TIME, LATITUDE, LONGITUDE, ALTITUDE, RPM='nullRPM', SPEED='nullkm/h', AMBIENTAIRTEMPERATURE='nullC', THROTTLEPOSITION='null%', ABSOLUTELOAD='null%', FUELLEVEL='null%'):
        data_to_insert = (car_id, TIME, LATITUDE, LONGITUDE, ALTITUDE, RPM[:-3], SPEED[:-4], AMBIENTAIRTEMPERATURE[:-1], THROTTLEPOSITION[:-1], ABSOLUTELOAD[:-1], FUELLEVEL[:-1])
        self.execute('''INSERT INTO raw_driving_data VALUES ("%s",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);''' % data_to_insert)

    def createProcessedData(self, car_id, ride_id, AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark):
        try:
            self.execute(f'''SELECT car_id, ride_id FROM processed_data WHERE car_id = "{car_id}" AND ride_id = {ride_id}''')
            count = self.cur.fetchall()
            if len(count) > 0:
                data_to_insert = (AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark, car_id, ride_id)
                self.execute('''UPDATE processed_data SET AverageSpeed = %s, AverageAcceleration = %s, RideDistance = %s, RideTime = %s, RideCost = %s, FuelConsumtion = %s, AccelerationCounter = %s, EngineRotation = %s, Efficiency = %s, LateralAcceleration = %s, DangerousAcceleration = %s, NegativeAcceleration = %s, SpeedCounter = %s, Security = %s, DrivingMark = %s WHERE car_id = "%s" AND ride_id = %s'''% data_to_insert)
            else:
                data_to_insert = (car_id, ride_id, AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark)
                self.execute('''INSERT INTO processed_data VALUES ("%s",%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);''' % data_to_insert)
        except Exception as e:
            raise Exception(e)
    
    def createRide(self, ride_id, car_id, time):
        try:
            self.execute(f'''INSERT INTO rides (car_id, ride_id, time) VALUES ("{car_id}", {ride_id}, {time});''')
            return True
        except Exception as e:
            raise Exception(e)

    def getUsers(self):
        try:
            self.execute('''SELECT * FROM users;''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCars(self):
        try:
            self.execute('''SELECT * FROM cars;''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCarModels(self):
        try:
            self.execute('''SELECT * FROM car_models;''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getRawDrivingData(self):
        try:
            self.execute("SELECT * FROM raw_driving_data")
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getProcessedData(self):
        try:
            self.execute('''SELECT * FROM processed_data;''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCarProcessedData(self, car_id):
        try:
            self.execute(f'''SELECT * FROM processed_data WHERE car_id="{car_id}";''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getUserInfo(self, user_id):
        try:
            self.execute(f'''SELECT * FROM users WHERE user_id = "{user_id}";''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getUserID(self, email):
        try:
            self.execute(f'''SELECT user_id FROM users WHERE email = "{email}";''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCarData(self, car_id):
        try :
            self.execute(f'''SELECT * FROM raw_driving_data WHERE car_id = "{car_id}";''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCarOwner(self, car_id):
        try:
            self.execute(f'''SELECT user_id, max(time) FROM cars GROUP BY car_id HAVING car_id = "{car_id}";''')
            collected_data = self.cur.fetchall()
            if len(collected_data) > 0:
                return collected_data[0]
            return ()
        except Exception as e:
            raise Exception(e)
    
    def getRide(self, car_id, ride_id):
        try:
            self.execute(f'''SELECT raw_driving_data.{', raw_driving_data.'.join(self.getColumns("raw_driving_data"))} FROM (rides JOIN raw_driving_data ON rides.car_id = raw_driving_data.car_id AND rides.time = raw_driving_data.time) WHERE rides.car_id = "{car_id}" AND rides.ride_id = {ride_id};''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getRideID(self, car_id, time):
        try:
            self.execute(f'''SELECT ride_id FROM rides  WHERE car_id = "{car_id}" AND time = {time};''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getRides(self, car_id):
        try:
            self.execute(f'''SELECT * FROM (rides JOIN raw_driving_data ON rides.car_id = raw_driving_data.car_id AND rides.time = raw_driving_data.time) WHERE rides.car_id = "{car_id}";''')
            collected_data = self.cur.fetchall()
            rides = []
            l = [collected_data[0]]
            for row in collected_data:
                if row[1] != l[0][1]:
                    rides.append(l)
                    l = [row]
                else:
                    l.append(row)
            rides.append(l)
            return rides
        except Exception as e:
            raise Exception(e)
        
    def getTables(self):
        try:
            self.execute('''SELECT name FROM sqlite_master WHERE type ='table' AND name NOT LIKE 'sqlite_%';''')
            collected_data = self.cur.fetchall()
            return [item[0] for item in collected_data]
        except Exception as e:
            raise Exception(e) 
    
    def getColumns(self,table_name):
        try:
            if table_name in self.getTables():
                self.execute(f'PRAGMA table_info({table_name});')
                collected_data = self.cur.fetchall()
                return [entry[1] for entry in collected_data]
            else:
                raise Exception("Table name not found")
        except Exception as e:
            raise Exception(e)

    def close(self):
        try:
            self.con.close()
        except Exception as e:
            raise Exception(e)
    
    def addData(self, data, user_id, time):
        try:
            lines = data.split(",\n")
            vin = lines[0].split(',')[0]
            for line in lines:
                if len(line) > 0:
                    if line[0] == "V":
                        vin = line.split(',')[0][1:]
                        self.addCar(vin, user_id, time)
                    else :
                        self.createRawDrivingData(vin,*line.split(','))
        except Exception  as e:
            if str(e)[:24] != "UNIQUE constraint failed":
                raise Exception(e)
    
    def addCar(self, car_id, user_id, time):
        try:
            owner = self.getCarOwner(car_id)
            if len(owner) > 0:
                if owner[0] != user_id:
                    self.createCar(car_id, user_id, time)
            else:
                self.createCar(car_id, user_id, time)
        except Exception  as e:
            if str(e)[:24] != "UNIQUE constraint failed":
                raise Exception(e)
    
    def addRideProcessedData(self, car_id, ride_id):
        try:
            AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark = self.processRideData(car_id, ride_id)
            self.createProcessedData(car_id, ride_id, AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark)
        except Exception  as e:
            raise Exception(e)
    
    def addCarProcessedData(self, car_id):
        try:
            rides = len(self.getRides(car_id))
            for ride_id in range(rides):
                self.addRideProcessedData(car_id, ride_id)
        except Exception as e:
            raise Exception(e)
    
    def processData(self):
        try:
            self.createRides()
            cars = self.getCarsOnly()
            for car in cars:
                self.addCarProcessedData(car[0])
        except Exception as e:
            raise Exception(e)
    
    def getTable(self,table_name):
        try:
            if table_name in self.getTables():
                self.execute(f'SELECT * FROM {table_name}')
                collected_data = self.cur.fetchall()
                return collected_data
            else:
                raise Exception("Table not found")
        except Exception as e:
            raise Exception(e)
    
    def getNumbers(self,titles):
        try:
            self.execute("SELECT count(DISTINCT car_id) FROM cars")
            cars_number = self.cur.fetchall()
            self.execute("SELECT DISTINCT count(user_id) FROM users")
            users_number = self.cur.fetchall()
            self.execute("SELECT count(DISTINCT ride_id) FROM rides GROUP BY car_id")
            rides_number = sum(x[0] for x in self.cur.fetchall())
            return [{"title":titles[0],"number":users_number[0][0]},{"title":titles[1],"number":cars_number[0][0]},{"title":titles[2],"number":rides_number}]
        except Exception as e:
            raise Exception(e)
    
    def getCarsOnly(self):
        try:
            self.execute('''SELECT DISTINCT car_id FROM cars;''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getUserProfile(self,user_id):
        try:
            self.execute(f'SELECT DISTINCT firstTable.car_id, firstTable.lastTime FROM (SELECT DISTINCT car_id, max(time) AS lastTime FROM cars GROUP BY car_id) AS firstTable JOIN (SELECT DISTINCT car_id, max(time) AS userTime FROM cars GROUP BY car_id HAVING user_id == "{user_id}") AS secondTable ON firstTable.car_id == secondTable.car_id AND firstTable.lastTime == secondTable.userTime;')
            collected_data = self.cur.fetchall()
            return {"columns":["car_id","time"],"data":collected_data}
        except Exception as e:
            raise Exception(e)
    
    def getCarProfile(self,car_id):
        try:
            rides = self.getRides(car_id)
            collected_data = [ [i, len(rides[i])] for i in range(len(rides))]
            columns = ["ride_id", "nbr_entries"]
            return {"columns":columns, "data":collected_data}
        except Exception as e:
            raise Exception(e)
    
    def getRideProfile(self,ride_id,car_id):
        try:
            collected_data = self.getRide(car_id, ride_id)
            columns = self.getColumnsConcise("raw_driving_data")
            return {"columns":columns, "data": collected_data}
        except Exception as e:
            raise Exception(e)
    
    def getTableConcise(self,table_name):
        try:
            if table_name in self.getTables():
                if table_name == "raw_driving_data":
                    s = ", ".join(data_columns)
                    self.execute(f'SELECT {s} FROM raw_driving_data')
                elif table_name == "processed_data":
                    s = ", ".join(processed_data_columns)
                    self.execute(f'SELECT {s} FROM processed_data')
                else:
                    self.execute(f'SELECT * FROM {table_name}')
                collected_data = self.cur.fetchall()
                return collected_data
            else:
                raise Exception("Table not found")
        except Exception as e:
            raise Exception(e)
    
    def getColumnsConcise(self, table_name):
        try:
            if table_name in self.getTables():
                if table_name == "raw_driving_data":
                    return data_columns
                elif table_name == "processed_data":
                    return processed_data_columns
                else:
                    self.execute(f'PRAGMA table_info({table_name});')
                    collected_data = self.cur.fetchall()
                    return [entry[1] for entry in collected_data]
            else:
                raise Exception("Table name not found")
        except Exception as e:
            raise Exception(e)
    
    def getSpeed(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT raw_driving_data.time, raw_driving_data.SPEED FROM raw_driving_data JOIN rides ON raw_driving_data.time == rides.time WHERE raw_driving_data.car_id = "{car_id}" AND rides.ride_id ={ride_id}''')
            collected_data = self.cur.fetchall()
            t = []
            v = []
            for i in range(len(collected_data)):
                t.append(collected_data[i][0])
                v.append(collected_data[i][1])
            return t, v
        except Exception as e:
            raise Exception(e)

    def getTimeMinTimeMax(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT MIN(time),MAX(time) FROM rides GROUP BY car_id, ride_id HAVING car_id="{car_id}" AND ride_id={ride_id}''')
            collected_data = self.cur.fetchall()
            return collected_data[0]
        except Exception as e:
            raise Exception(e)

    def getFuelLevel(self, ride_id, car_id, time):
        try:
            self.execute(f'''SELECT raw_driving_data.FUELLEVEL FROM raw_driving_data JOIN rides ON raw_driving_data.time == rides.time WHERE raw_driving_data.car_id = "{car_id}" AND rides.ride_id ={ride_id} AND rides.time ={time}''')
            collected_data =  self.cur.fetchall()
            return collected_data[0][0]
        except Exception as e:
            raise Exception(e)

    def getFuelConsumption(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT raw_driving_data.FUELLEVEL FROM raw_driving_data JOIN rides ON raw_driving_data.time == rides.time WHERE raw_driving_data.car_id = "{car_id}" AND rides.ride_id ={ride_id}''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getRPM(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT raw_driving_data.RPM FROM raw_driving_data JOIN rides ON raw_driving_data.time == rides.time WHERE raw_driving_data.car_id = "{car_id}" AND rides.ride_id ={ride_id}''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)

    def getTimeRide(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT time FROM rides WHERE car_id = "{car_id}" AND ride_id = {ride_id}''')
            collected_data = self.cur.fetchall()
            minimum = self.getTimeMinTimeMax(ride_id, car_id)[0]
            t = [collected_data[i][0] - minimum for i in range(len(collected_data))]
            return t
        except Exception as e:
            raise Exception(e)
    
    def getMetrics(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT data.time, data.SPEED, data.RPM, data.FUELLEVEL FROM (SELECT * FROM raw_driving_data WHERE car_id = "{car_id}") AS data JOIN (SELECT * FROM rides WHERE car_id = "{car_id}" AND ride_id = {ride_id}) AS ride ON data.time == ride.time''')
            collected_data = self.cur.fetchall()

            time = []
            v = []
            rpm = []
            fuel = []
            alat = []

            minimum = self.getTimeMinTimeMax(ride_id, car_id)[0]

            for i in range(len(collected_data)):
                time.append(collected_data[i][0] - minimum)
                v.append(collected_data[i][1])
                rpm.append(collected_data[i][2])
                fuel.append(collected_data[i][3])
                alat.append(0)

            return time, v, rpm, fuel, alat
        except Exception as e:
            raise Exception(e)

    def getRideProcessedData(self, ride_id, car_id):
        try:
            self.execute(f'''SELECT * FROM processed_data WHERE car_id="{car_id}" AND ride_id={ride_id}''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def getCarProcessedData(self, car_id):
        try:
            self.execute(f'''SELECT * FROM processed_data WHERE car_id="{car_id}"''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def processRideData(self, car_id, ride_id):
        try:
            time, speed = self.getSpeed(ride_id, car_id)
            acceleration, t = derivate(time, speed, K)
            times = self.getTimeMinTimeMax(ride_id, car_id)
            initial_fuel_level = (self.getFuelLevel(ride_id, car_id, times[0]))
            final_fuel_level = (self.getFuelLevel(ride_id, car_id, times[1]))
            consumed_fuel = abs(initial_fuel_level-final_fuel_level)*FUEL_CAPACITY/CONSUMED_FUEL
            
            RideTime = (times[1] - times[0]) / K
            AverageSpeed = average(speed)
            AverageAcceleration = average(acceleration)
            RideDistance = AverageSpeed*RideTime
            RideCost = consumed_fuel*float(prix_carburant['SP95']) 

            FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark = driving_marks(self, ride_id, car_id)
            
            return AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark
        except Exception as e:
            raise Exception(e)
    
    def getLastRide(self, car_id):
        try:
            self.execute(f'''SELECT max(ride_id) FROM rides GROUP BY car_id HAVING car_id="{car_id}";''')
            collected_data = self.cur.fetchall()
            return collected_data
        except Exception as e:
            raise Exception(e)
    
    def dataForPlotly(self, car_id, ride_id):
        try:
            time, speed = self.getSpeed(ride_id, car_id)
            acceleration, t = derivate(time, speed, 1000)
            times = self.getTimeMinTimeMax(ride_id, car_id)
            initial_fuel_level = (self.getFuelLevel(ride_id, car_id, times[0]))
            final_fuel_level = (self.getFuelLevel(ride_id, car_id, times[1]))
            consumed_fuel = abs(initial_fuel_level-final_fuel_level)*FUEL_CAPACITY/CONSUMED_FUEL
            
            RideTime = (times[1] - times[0]) / K
            AverageSpeed = average(speed)
            AverageAcceleration = average(acceleration)
            RideDistance = AverageSpeed*RideTime
            RideCost = consumed_fuel*float(prix_carburant['SP95']) 

            FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark = driving_marks(self, ride_id, car_id)
            
            return time, speed, acceleration, AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark
        except Exception as e:
            raise Exception(e)