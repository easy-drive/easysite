from flask import json, make_response
from constants import *
from notation import evaluation
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from datetime import datetime
import plotly.offline
import gzip

def profiles(request, data_base):
        try:
                titles = ["Users","Cars","Rides"]
                profileTitles = ["UserProfile","CarProfile","RideProfile"]
                request_data = request.get_json()
                op = request_data.get("operation")
                if op == "getNumbers":
                        response = data_base.getNumbers(titles)
                        return json.jsonify(response)
                elif op in titles:
                        if op == titles[0]:
                                data = data_base.getTable("users")
                                columns = data_base.getColumns("users")
                        elif op == titles[1]:
                                data = data_base.getCarsOnly()
                                columns = ["car_id"]
                        elif op == titles[2]:
                                data = data_base.getTable("rides")
                                columns = data_base.getColumns("rides")
                        else:
                                return "None"
                        content = gzip.compress(json.dumps({'data': data, 'columns': columns}).encode('utf8'), 5)
                        response = make_response(content)
                        response.headers['Content-length'] = len(content)
                        response.headers['Content-Encoding'] = 'gzip'
                        return response
            
                elif op in profileTitles:
                        if op == profileTitles[0]:
                                data = data_base.getUserProfile(request_data.get("user_id"))
                        elif op == profileTitles[1]:
                                data = data_base.getCarProfile(request_data.get("car_id"))
                        elif op == profileTitles[2]:
                                data = data_base.getRideProfile(request_data.get("ride_id"), request_data.get("car_id"))
                        else:
                                return "None"
                        content = gzip.compress(json.dumps(data).encode('utf8'), 5)
                        response = make_response(content)
                        response.headers['Content-length'] = len(content)
                        response.headers['Content-Encoding'] = 'gzip'
                        return response
        except Exception as e:
                print(e)
                return "None"

def getDb(request, data_base):
        try:
                request_data = request.get_json()
                data = data_base.getTableConcise(request_data.get('table'))
                columns = data_base.getColumnsConcise(request_data.get('table'))
                content = gzip.compress(json.dumps({'data': data, 'columns': columns}).encode('utf8'), 5)
                response = make_response(content)
                response.headers['Content-length'] = len(content)
                response.headers['Content-Encoding'] = 'gzip'
                return response
        except Exception as e:
                print(e)
                return "None"

def processDb(request, data_base):
        try:
                request_data = request.get_json()
                data_base.processData()
                data = data_base.getTableConcise(request_data.get('table'))
                columns = data_base.getColumnsConcise(request_data.get('table'))
                content = gzip.compress(json.dumps({'data': data, 'columns': columns}).encode('utf8'), 5)
                response = make_response(content)
                response.headers['Content-length'] = len(content)
                response.headers['Content-Encoding'] = 'gzip'
                return response
        except Exception as e:
                print(e)
                return "None"

def sendMarks(request, data_base):
        try:
                request_data = request.get_json()
                user_id = request_data['user_id']
                cars = data_base.getUserProfile(user_id)
                car_id = cars['data'][-1][0]
                raw_data = data_base.getCarData(car_id)
                processed_data = data_base.getCarProcessedData(car_id)
                raw_columns = data_base.getColumns('raw_driving_data')
                processed_columns = data_base.getColumns('processed_data')
                if len(raw_data) > 100:
                        raw_data = raw_data[:100]
                response = json.dumps({'raw_data':{'data': raw_data, 'columns': raw_columns}, 'processed_data': {'data': processed_data, 'columns': processed_columns}})
                return response
        except Exception as e:
                print(e)
                return "None"

def getMarks(request, data_base):
        try:
                request_data = request.get_json()
                car_id = request_data['car_id']
                processed_data = data_base.getCarProcessedData(car_id)
                if len(processed_data) > 0:
                        processed_data = processed_data[-1]
                processed_columns = data_base.getColumns('processed_data')
                return json.dumps({'processed_data': processed_data, 'columns': processed_columns})
        except Exception as e:
                print(e)
                return "None"

def graphs(data_base, ride_id, car_id):
        fig = make_subplots(
                rows=3, cols=6,
                specs=[
                [{"type": "indicator", "colspan": 1}, {"type": "indicator", "colspan": 1}, {"type": "indicator", "colspan": 1}, {"type": "bar", "colspan": 3, "rowspan": 2}, None, None],
                [{"type": "scatter", "colspan": 3}, None, None,None, None, None],
                [{"type": "scatter", "colspan": 3}, None, None, {"type": "indicator", "colspan": 1},{"type": "indicator", "colspan": 1} , {"type": "indicator", "colspan": 1}]]
        )

        time, speed, acceleration, AverageSpeed, AverageAcceleration, RideDistance, RideTime, RideCost, FuelConsumtion, AccelerationCounter, EngineRotation, Efficiency, LateralAcceleration, DangerousAcceleration, NegativeAcceleration, SpeedCounter, Security, DrivingMark = data_base.dataForPlotly(car_id, ride_id)
        
        x = []

        for t in time[len(time)-N:len(time)]:
                dt = datetime.fromtimestamp(t/1000)
                x += [dt.strftime("%Y-%m-%d %H:%M:%S")]


        fig.add_trace(
                go.Indicator(
                mode="number",
                value=AverageSpeed,
                title="Average Speed",
                ),
                row=1, col=1
        )

        fig.add_trace(
                go.Indicator(
                mode="number",
                value=RideCost,
                title="Travel Cost",
                ),
                row=1, col=2
        )

        fig.add_trace(
                go.Indicator(
                mode="number",
                value=RideDistance,
                title="Total Distance per ride",
                ),
                row=1, col=3
        )

        fig.add_trace(
                go.Scatter(
                x=x,
                y=speed[-N:],
                name="Speed",
                marker=dict(color="Yellow"),
                showlegend=True,
                ),
                row=2, col=1
        )

        fig.add_trace(
                go.Scatter(
                x=x,
                y=acceleration[-N:],
                name="Acceleration",
                marker=dict(color="Green")
                ),
                row=3, col=1
        )

        fig.add_trace(
                go.Scatter(
                x=x,
                y=[51]*N,
                marker=dict(color="Orange"),
                name="Speed Limit",
                ),
                row=2, col=1
        )

        fig.add_trace(
                go.Bar(
                name='1',
                x=['ecodriving', 'security'],
                y=[int(AccelerationCounter*M), int(DangerousAcceleration*M)],
                marker_color=['yellow', 'orange'],
                showlegend=False,
                text=['Acceleration mark', 'Dangerous acc mark']
                ),
                row=1, col=4
        )

        fig.add_trace(
                go.Bar(
                name='2',
                x=['ecodriving', 'security'],
                y=[int(FuelConsumtion*M), int(NegativeAcceleration*M)],
                marker_color=['lightgreen', 'green'],
                showlegend=False,
                text=['Fuel consumption mark', 'Negative acc mark']
                ),
                row=1, col=4
        )

        fig.add_trace(
                go.Bar(
                name='3',
                x=['ecodriving', 'security'],
                y=[int(EngineRotation*M), int(SpeedCounter*M)],
                marker_color=['pink', 'crimson'],
                showlegend=False,
                text=['Rpm mark', 'Speed mark']
                ),
                row=1, col=4
        )

        fig.add_trace(
                go.Indicator(
                mode="number",
                value=int(Efficiency*M),
                title={"text": "EcoDriving Mark"},
                number={"font":{"size":INDICATOR_FONT_SIZE}}
                ),
                row=3, col=4
        )
        fig.add_trace(
                go.Indicator(
                mode="number",
                value=int(Security*M),
                title={"text":"Security Mark"},
                number={"font":{"size":INDICATOR_FONT_SIZE}}
                ),
                row=3, col=6
        )
        
        fig.add_trace(
                go.Indicator(
                mode="number",
                value=int(DrivingMark*M),
                title={"text": "Driving Mark"},
                number={"font":{"size":INDICATOR_FONT_SIZE}}
                ),
                row=3, col=5
        )
        fig.update_layout(
                height=650,
                template="plotly_white",
                showlegend=True,
                legend_orientation="h",
                legend=dict(x=0.2, y=0.7)
        )

        div = plotly.offline.plot(fig, include_plotlyjs=False, output_type='div')
        return div
