from prix_carburant import *
from constants import *

# https://lcsi.umh.es/docs/pfcs/PFC_TFG_Bocanegra_Fernando.pdf

def derivate(time, x, l=1):
    t = []
    d = []
    for i in range(1, len(time) - 1):
        t.append((time[i+1] + time[i-1])/(2*l))
        d.append(l*(x[i+1] - x[i-1])/(time[i+1] - time[i-1]))
    return d, t

def integrate(time, x, l=1):
    integral = 0
    for i in range(1, len(x)):
        integral += (x[i] + x[i-1]) * (time[i] - time[i-1])/(2*l)
    return integral

def average(x):
    return sum(x) / len(x)

def averageRideSpeed(db, ride_id, car_id):
    time, speed = db.getSpeed(ride_id, car_id)
    return average(speed)

def averageRideAcceleration(db, ride_id, car_id):
    time, speed = db.getSpeed(ride_id, car_id)
    acceleration, t = derivate(time, speed, K)
    return average(acceleration)

def travelDuration(db, ride_id, car_id):
    times = db.getTimeMinTimeMax(ride_id, car_id)
    return (times[1] - times[0]) / K

def travelledDistance(db, ride_id, car_id):
    average_speed = averageRideSpeed(db, ride_id, car_id)
    duration = travelDuration(db, ride_id, car_id)
    return average_speed * duration

def exceedingTravelDuration(db, ride_id, car_id):
    return max(0, travelDuration(db, ride_id, car_id) - TIME_LIMIT)

def travelCost(db, ride_id, car_id):
    times = db.getTimeMinTimeMax(ride_id, car_id)
    initial_fuel_level = (db.getFuelLevel(ride_id, car_id, times[0]))
    final_fuel_level = (db.getFuelLevel(ride_id, car_id, times[1]))
    consumed_fuel = abs(initial_fuel_level-final_fuel_level)*FUEL_CAPACITY/CONSUMED_FUEL
    cost = consumed_fuel*float(prix_carburant['SP95'])
    return cost

if __name__ == '__main__':
    car_id = "WP0ZZZ99ZTS390000"
    ride_id = 0
    db = DataBase()
    print("Average speed :", averageRideSpeed(db, ride_id, car_id), "km/h")
    print("Average acceleration :", averageRideAcceleration(db, ride_id, car_id), "km/h2")
    print("Distance parcourure :", travelledDistance(db, ride_id, car_id), "km")
    print("Travel duration :", travelDuration(db, ride_id, car_id))
    print("Travel cost :", travelCost(db, ride_id, car_id), "€")
